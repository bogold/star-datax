package com.alibaba.datax.plugin.reader.prestoreader;

public class Constant
{

    public static final int DEFAULT_FETCH_SIZE = 1000;

    private Constant() {}
}
